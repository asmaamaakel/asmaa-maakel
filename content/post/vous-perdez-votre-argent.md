+++
date = "2021-02-15"
title = "Vous perdez votre argent!"

+++

Quantité ou qualité? Que privilégiez-vous?
Choisir le candidat le moins coûteux peut paraître un moyen de réduction du coût d’une société et plus d’heures au bureau peut paraître un indicateur de productivité en entreprise. Et si je vous disais que ces indicateurs étaient dépassés ? En effet, ces deux indicateurs sont issus de modèles économiques dépassés. Clin d’œil au Fordisme et au Taylorisme. Les bons profils se valent. Un budget restreint implique un choix réduit et donc des profils mauvais ou peu motivés coûtent cher à une entreprise sur le long terme. Moins d’heures = plus d’emplois et des employé plus épanouis et plus motivés impliquent plus de productivité. Pourquoi ces pratiques sont toujours d’actualité me direz-vous, eh bien parce qu’elles profitent encore à certains car 1% du monde détient 99% de sa richesse. Alors oui, le marché de l’emploi grouille de profils, mais la qualité y fait défaut, cela permet de maintenir un rapport de force, mais détrompez-vous ! Si vous pensez vous faire beaucoup d’argent ainsi, vous pourrez vous enrichir encore plus en optant pour une approche plus souple sans même avoir à être sur le dos de vos employés. Travailler moins en entreprise ne signifie pas moins de rendement. Contrairement à quand on est contraint de travailler, quand on est passionnés et épanouis, on a du mal à s’arrêter de faire la chose qu’on aime et à ce moment-là, vos employés vous le rendront ! Microsoft et Unilever l’ont compris et de plus en plus de géants, adoptent la semaine des 4 jours ainsi que des packs salariaux, très compétitifs. Et maintenant, vous préférez la qualité ou la quantité? 




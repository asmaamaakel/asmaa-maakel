+++
menu = "main"
title = "À Propos"
type = "about"
weight = 10
+++

Multipotentielle, Passionnée et ayant toujours la soif d'apprendre. J'ai eu un parcours éducatif assez riche entre informatique et marketing. Et j'ai travaillé pour des clients de différents secteurs et différentes tailles, des multinationales jusqu'aux start-ups.

![about](../images/mac.jpg)

### ÉDUCATION

MBA en Marketing Management, je suis tout d'abord issue d'un parcours en informatique conclu par une licence. Aussi, certifiée Google et Hubspot.

### EXPÉRIENCES

J'ai été RH & Stock manager dans une branche de Best Western avant d'être Marketing Manager puis Business Manger chez une start-up dans le secteur des énergies ayant travaillé sur des contrats pour des multinationales de différentes nationalités.

### PROJETS

Actuellement Community Manager et Développeur Web en freelance.